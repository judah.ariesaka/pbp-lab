class Artikel {
  final String id;
  final String judul;
  final String ringkasan;
  
  const Artikel({
    @required this.id;
    @required this.judul;
    @required this.ringkasan;
  });
}