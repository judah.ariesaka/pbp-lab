import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

final List<Widget> imageSliders = imgList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1000.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'Artikel No. ${imgList.indexOf(item) + 1}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Artikel',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.indigo,
          canvasColor: Color.fromRGBO(255, 254, 229, 1),
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              )),
        ),
        home: MainPage());
  }
}

// https://pub.dev/packages/carousel_slider/example
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Artikel"),
            leading: Icon(
              Icons.menu,
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {},
              )
            ]),
        body: ListView(children: <Widget>[
          Container(
              margin: EdgeInsets.all(15),
              child: SizedBox(
                child: Text(
                  "Artikel Terpopuler",
                  style: Theme.of(context).textTheme.headline6,
                ),
              )),
          Container(
              child: CarouselSlider(
            options: CarouselOptions(
              aspectRatio: 2.0,
              enlargeCenterPage: true,
              enableInfiniteScroll: false,
              initialPage: 2,
              autoPlay: true,
            ),
            items: imageSliders,
          )),
          Container(
              margin: EdgeInsets.all(15),
              child: SizedBox(
                child: Text("Baca artikel hari ini",
                    style: Theme.of(context).textTheme.headline6),
              )),
          Row(children: <Widget>[
            Flexible(
                flex: 1,
                child: Container(
                    width: 200,
                    height: 250,
                    margin: EdgeInsets.all(10),
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        child: Card(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                child:
                                    Image.network(imgList[0], fit: BoxFit.fill),
                              ),
                              Container(
                                  margin: EdgeInsets.all(10),
                                  child: SizedBox(
                                    child: Text("Artikel 1",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: SizedBox(
                                      child: Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis urna quis felis vehicula dignissim.",
                                    style: TextStyle(fontSize: 10),
                                    textAlign: TextAlign.justify,
                                  ))),
                            ]))))),
            Flexible(
                flex: 1,
                child: Container(
                    width: 200,
                    height: 250,
                    margin: EdgeInsets.all(10),
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        child: Card(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                child:
                                    Image.network(imgList[1], fit: BoxFit.fill),
                              ),
                              Container(
                                  margin: EdgeInsets.all(10),
                                  child: SizedBox(
                                    child: Text("Artikel 2",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: SizedBox(
                                      child: Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis urna quis felis vehicula dignissim.",
                                    style: TextStyle(fontSize: 10),
                                    textAlign: TextAlign.justify,
                                  ))),
                            ]))))),
          ]),
          Row(children: <Widget>[
            Flexible(
                flex: 1,
                child: Container(
                    width: 200,
                    height: 250,
                    margin: EdgeInsets.all(10),
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        child: Card(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                child:
                                    Image.network(imgList[2], fit: BoxFit.fill),
                              ),
                              Container(
                                  margin: EdgeInsets.all(10),
                                  child: SizedBox(
                                    child: Text("Artikel 3",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: SizedBox(
                                      child: Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis urna quis felis vehicula dignissim.",
                                    style: TextStyle(fontSize: 10),
                                    textAlign: TextAlign.justify,
                                  ))),
                            ]))))),
            Flexible(
                flex: 1,
                child: Container(
                    width: 200,
                    height: 250,
                    margin: EdgeInsets.all(10),
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        child: Card(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.0)),
                                child:
                                    Image.network(imgList[3], fit: BoxFit.fill),
                              ),
                              Container(
                                  margin: EdgeInsets.all(10),
                                  child: SizedBox(
                                    child: Text("Artikel 4",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              Container(
                                  margin:
                                      EdgeInsets.symmetric(horizontal: 10.0),
                                  child: SizedBox(
                                      child: Text(
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mattis urna quis felis vehicula dignissim.",
                                    style: TextStyle(fontSize: 10),
                                    textAlign: TextAlign.justify,
                                  ))),
                            ]))))),
          ])
        ]),
        floatingActionButton:
            FloatingActionButton(child: Icon(Icons.add_rounded)));
  }
}
