import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form Flutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  String selected;
  List<String> kategori = [
    "Kesehatan Anak",
    "Coronavirus",
    "Kesehatan Hewan",
    "Penyakit Dalam",
  ];

  List<DropdownMenuItem> generateItems(List<String> kategori) {
    List<DropdownMenuItem> items = [];
    for (var item in kategori) {
      items.add(DropdownMenuItem(child: Text(item), value: item));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tulis Artikelmu"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "Masukkan link foto artikel",
                      labelText: "Foto",
                      icon: Icon(Icons.add_photo_alternate),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Link foto tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLength: 144,
                    decoration: new InputDecoration(
                      hintText: "Tuliskan judul artikel",
                      labelText: "Judul",
                      icon: Icon(Icons.text_rotation_none_rounded),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Judul artikel tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.all(8.0),
                    // https://www.youtube.com/watch?v=kyu4711PvUw
                    child: DropdownButtonFormField(
                      icon: Icon(Icons.view_list),
                      hint: Text("Kategori"),
                      value: selected,
                      items: generateItems(kategori),
                      onChanged: (item) {
                        setState(() {
                          selected = item;
                        });
                      },
                    )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLines: null,
                    decoration: new InputDecoration(
                      hintText: "Tuliskan isi artikel",
                      labelText: "Isi",
                      icon: Icon(Icons.text_snippet_rounded),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Isi artikel tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    maxLength: 256,
                    decoration: new InputDecoration(
                      hintText: "Tuliskan ringkasan artikel",
                      labelText: "Ringkasan",
                      icon: Icon(Icons.textsms_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ringkasan artikel tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      var snackBar =
                          SnackBar(content: Text("Artikel berhasil disimpan"));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      // https://stackoverflow.com/questions/40579879/display-snackbar-in-flutter
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
