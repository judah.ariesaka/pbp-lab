# Generated by Django 3.2.7 on 2021-09-16 12:32

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='birth_date',
            field=models.DateField(default=django.utils.timezone.now, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='friend',
            name='npm',
            field=models.CharField(default=django.utils.timezone.now, max_length=10),
            preserve_default=False,
        ),
    ]
