1. Apakah perbedaan antara JSON dan XML?
JSON dan XML memiliki struktur penulisan yang berbeda. JSON umumnya menggunakan struktur map {"key":"value"} dimana setiap value juga bisa berbentuk struktur yang sama {"key": {"key":"value"}}. Sedangkan XML memiliki struktur yang mirip dengan HTML, yaitu dengan menggunakan opening dan closing tags untuk setiap data. Hal ini membuat JSON cenderung lebih mudah dibaca dan ringan karena tidak memerlukan closing tags.

XML memiliki atribut pada tagsnya yang membuat parsing pada XML bisa menjadi lebih spesifik dibanding JSON yang tidak memiliki fitur ini sehingga XML cocok untuk mengolah data dalam jumlah yang banyak, meski kecepatannya lambat.

Ada juga perbedaan-perbedaan lain antara JSON dan XML, seperti JSON mendukung penggunaan array sedangkan XML tidak. JSON hanya mendukung UTF-8 encoding, sedangkan XML mendukung berbagai tipe encoding. Ukuran dokumen file XML juga lebih besar dibanding JSON.


2. Apakah perbedaan antara HTML dan XML?
Meski keduanya merupakan Markup Language, bahasa dokumen yang dicirikan dengan penggunaan tags, HTML dan XML memiliki beberapa perbedaan.

Berdasarkan fungsi utamanya, HTML digunakan untuk menampilkan data (dalam website) sedangkan XML lebih digunakan untuk keperluan transport data, seperti menyimpan dan menyalurkan dari database.

Selain itu, HTML memiliki tags yang diatur oleh kesepakatan bersama, khususnya stakeholder/perusahaan besar seperti Facebook, Twitter atau Instagram untuk kepentingan mereka. Sedangkan XML, tags-nya bersifat self-descriptive sehingga siapapun bisa membuat tagsnya sendiri agar data/informasi yang disimpan dan disampaikan bisa dimengerti .

Syntax keduanya mirip karena sama-sama Markup Language, tetapi dalam HTML terdapat bagian-bagian seperti root, head dan body dan dalam XML terdapat bagian-bagian seperti root, branch dan leaves.

Ada juga beberapa perbedaan-perbedaan lain antara HTML dan XML, seperti tags dalam HTML case-insensitive sedangkan XML case-sensitive. Seluruh tags dalam XML wajib memiliki closing tags, sedangkan dalam HTML terdapat tags yang tidak memerlukan closing tags seperti (br).

Referensi:
- https://www.geeksforgeeks.org/html-vs-xml/
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://gitlab.com/PBP-2021/pbp-lab/-/tree/master/lab_instruction/lab_2
- https://hackr.io/blog/json-vs-xml