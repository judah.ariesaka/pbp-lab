from django import forms
from django.db.models import fields

from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'

    # input_attrs1 = {
	# 	'type' : 'text',
	# 	'placeholder' : 'Nama Kamu'
	# }

    # input_attrs2 = {
	# 	'type' : 'text',
	# 	'placeholder' : 'Nama Kamu'
	# }

    # input_attrs3 = {
	# 	'type' : 'date',
	# }

    # name = forms.CharField(label='',required=False,max_length=30,widget=forms.TextInput(attrs=input_attrs1))
    # npm = forms.CharField(label='',required=False, max_length=30, widget=forms.TextInput(attrs=input_attrs2))
    # birth_date= forms.DateField(label='',required=False, widget=forms.TextInput(attrs=input_attrs3))